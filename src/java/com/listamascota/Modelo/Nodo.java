/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listamascota.Modelo;

/**
 *
 * @author marthalucia
 */
public class Nodo{
    
    public Perro dato;
    public Nodo siguiente;
    public Nodo anterior;

    public Nodo(Perro dato) {
        this.dato = dato;
        siguiente = null;
        anterior = null;
    }    

    public Perro getDato() {
        return dato;
    }

    public void setDato(Perro dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    public Nodo getAnterior() {
        return anterior;
    }

    public void setAnterior(Nodo anterior) {
        this.anterior = anterior;
    }

      
}