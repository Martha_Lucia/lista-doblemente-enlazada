/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listamascota.Modelo;

import java.io.Serializable;


/**
 *
 * @author marthalucia
 */

    
    public class Perro implements Serializable {
    private String nombre;
    private byte edad;
    private byte numeroPulgas;
    String raza;

    public Perro(String nombre, byte edad, byte numeroPulgas, String raza) {
        this.nombre = nombre;
        this.edad = edad;
        this.numeroPulgas = numeroPulgas;
        this.raza = raza;
    }
    
   
    
    public Perro() {
    
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte getEdad() {
        return edad;
    }

    public void setEdad(byte edad) {
        this.edad = edad;
    }

    public byte getNumeroPulgas() {
        return numeroPulgas;
    }

    public void setNumeroPulgas(byte numeroPulgas) {
        this.numeroPulgas = numeroPulgas;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public String toString() {
        return "Perro{" + "nombre=" + nombre + ", edad=" + edad + ", numeroPulgas=" + numeroPulgas + ", raza=" + raza + '}';
    }
    
}
    

