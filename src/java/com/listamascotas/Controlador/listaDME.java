/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listamascotas.Controlador;

import com.listamascota.Modelo.Nodo;
import com.listamascota.Modelo.Perro;
import java.io.Serializable;

/**
 *
 * @author marthalucia
 */
public class listaDME implements Serializable {

    private Nodo cabeza;
    private Nodo fin;

    public listaDME() {
        cabeza = null;
    }

    public boolean esVacia() {
        return (this.cabeza == null);
    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    public Nodo getFin() {
        return fin;
    }

    public void setFin(Nodo fin) {
        this.fin = fin;
    }

    public String listarNodo() {
        String listado = "";
        if (cabeza == null) {
            return "La lista está vacía";
        } else {
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();
            }
            return listado;
        }
    }

    public Nodo obtenerNodoxPosicion(int pos) {
        if (cabeza != null) {
            Nodo temp = cabeza;
            int cont = 1;
            while (temp != null) {
                if (pos == cont) {

                    return new Nodo(temp.getDato());
                }
                temp = temp.getSiguiente();
                cont++;
            }
        }
        return null;
    }

    public void insertarCabecera(Perro dato) {
        Nodo nuevo = new Nodo(dato);
        if (esVacia()) {
            cabeza = nuevo;
        }
    }

    public Nodo obtenerUltimoNodo() {
        if (cabeza != null) {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }

    public String adicionarInicio(Perro dato) {
        Nodo nuevo = new Nodo(dato);
        if (cabeza == null) {
            cabeza = nuevo;
        } else {
            cabeza.setAnterior(nuevo);
            cabeza.getAnterior().setSiguiente(cabeza);
            cabeza = (Nodo) cabeza.getAnterior();
        }
        return "se adiciono con exito ";
    }

    public void adicionarFinal(Perro dato) {

        Nodo nuevo = new Nodo(dato);
        Nodo temporal = cabeza;
        if (this.cabeza == null) {

            cabeza = new Nodo(dato);

        } else {
            while (temporal.siguiente != null) {
                temporal = temporal.siguiente;
            }
            temporal.siguiente = nuevo;
            nuevo.anterior = temporal;
        }
    }

    public void eliminarInicio() {

        if (!esVacia()) {
            if (cabeza.siguiente != null) {
                cabeza = cabeza.siguiente;
                cabeza.anterior = null;

            } else {
                if (cabeza.anterior == null & cabeza.siguiente == null) {
                    cabeza = cabeza.siguiente;
                }
            }

        }
    }

    public void eliminarFinal() {
        Nodo temporal = cabeza;

        while (temporal.siguiente.siguiente != null) {
            temporal = temporal.siguiente;
        }

        temporal.siguiente = null;

    }

    public int tamañoNodo() {
        int contador = 0;
        Nodo temporal = cabeza;
        if (!esVacia()) {

            while (temporal != null) {
                temporal = temporal.siguiente;
                contador++;
            }
        }
        return contador;

    }

    public void eliminarPosicion(int posicion) {
        Nodo temporal = cabeza;
        int pos = 1;
        if (!esVacia()) {
            if (posicion == 1) {
                eliminarInicio();
            } else {
                if (posicion == tamañoNodo()) {
                    eliminarFinal();
                } else {
                    if (posicion > 0 && posicion < tamañoNodo()) {
                        while (pos != posicion) {
                            temporal = temporal.siguiente;
                            pos++;
                        }
                        temporal.anterior.siguiente = temporal.siguiente;
                        temporal.siguiente.anterior = temporal.anterior;
                        temporal.siguiente = null;
                        temporal.anterior = null;
                    }
                }

            }

        }
    }

    public void cambiarLista() {

        listaDME listaCopia = new listaDME();

        Nodo temp = this.cabeza;

        while (temp != null) {

            listaCopia.adicionarInicio(temp.getDato());

            temp = temp.getSiguiente();

        }

        this.cabeza = listaCopia.getCabeza();

    }

    public void obtenervocal() {
        Nodo temporal;
        listaDME listaCopia = new listaDME();
        temporal = this.cabeza;
        while (temporal != null) {
            if (temporal.getDato().getNombre().startsWith("A")
                    || temporal.getDato().getNombre().startsWith("E")
                    || temporal.getDato().getNombre().startsWith("I")
                    || temporal.getDato().getNombre().startsWith("O")
                    || temporal.getDato().getNombre().startsWith("U")
                    || temporal.getDato().getNombre().startsWith("a")
                    || temporal.getDato().getNombre().startsWith("e")
                    || temporal.getDato().getNombre().startsWith("i")
                    || temporal.getDato().getNombre().startsWith("o")
                    || temporal.getDato().getNombre().startsWith("u")) {

                listaCopia.adicionarInicio(temporal.getDato());

            } else {
                listaCopia.adicionarFinal(temporal.getDato());
            }
            temporal = temporal.getSiguiente();

        }
        this.cabeza = listaCopia.getCabeza();
    }
}


