/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listamascotas.Controlador;

import com.listamascota.Modelo.Nodo;
import com.listamascota.Modelo.Perro;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author marthalucia
 */
@Named(value = "listaMascotasController")
@SessionScoped
public class ListaMascotasController implements Serializable {
        
    private listaDME lista = new listaDME();


    private Nodo temp;
    private Perro PerroAdicionar;
    private boolean verNuevo = false;
    private boolean verBuscar = false;
    private String listado;
    private Perro perroEncontrado;
    private int posicionBuscar = 0;


    public boolean isVerBuscar() {
        return verBuscar;
    }

    public Perro getPerroEncontrado() {
        return perroEncontrado;
    }

    public int getPosicionBuscar() {
        return posicionBuscar;
    }

    public void setPosicionBuscar(int posicionBuscar) {
        this.posicionBuscar = posicionBuscar;
    }

    public void setPerroEncontrado(Perro perroEncontrado) {
        this.perroEncontrado = perroEncontrado;
    }

    public void setVerBuscar(boolean verBuscar) {
        this.verBuscar = verBuscar;
    }

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }

    public listaDME getLista() {
        return lista;
    }

    public void setLista(listaDME lista) {
        this.lista = lista;
    }

    public Nodo getTemp() {
        return temp;
    }

    public void setTemp(Nodo temp) {
        this.temp = temp;
    }

    public Perro getPerroAdicionar() {
        return PerroAdicionar;
    }

    public void setPerroAdicionar(Perro PerroAdicionar) {
        this.PerroAdicionar = PerroAdicionar;
    }

    public boolean isVerNuevo() {
        return verNuevo;
    }

    public void setVerNuevo(boolean verNuevo) {
        this.verNuevo = verNuevo;
    }

    
    public void adicionarPerroAlInicio(String nombre, byte edad, byte NumeroPulgas, String raza) {
        Perro nuevo = new Perro(nombre, edad, NumeroPulgas, raza);

        lista.adicionarInicio(nuevo);

    }

    public void adicionarPerroFinal(String nombre, byte edad, byte NumeroPulgas, String raza) {
        Perro nuevo = new Perro(nombre, edad, NumeroPulgas, raza);

        lista.adicionarFinal(nuevo);

    }

    public ListaMascotasController()
    {


        mostrarListado();
        temp = lista.getCabeza();
    }

    public void habilitarBuscar() {
        verBuscar = true;
    }

    public void mostrarListado() {
        listado = lista.listarNodo();
    }

    public void verCrearPerro() {
        verNuevo = true;
        PerroAdicionar = new Perro();
    }

    public void irAlsiguiente() {
        if (temp.getSiguiente() != null) {
            temp = temp.getSiguiente();
        }
    }

    public void irAnterior() {
        if (temp.getAnterior()!= null) {
            temp = temp.getAnterior();
        }
    }

    public void irAlPrimero() {
        temp = lista.getCabeza();
    }

    public void irAlUltimo() {
        Nodo ultimo = lista.obtenerUltimoNodo();
        if (ultimo != null) {
            temp = ultimo;
        }
    }
    
   


    public void buscarPorPosicion() {
        perroEncontrado = lista.obtenerNodoxPosicion(posicionBuscar)
                .getDato();
    }

    public void adicionarAlInicio() {
        lista.adicionarInicio(PerroAdicionar);
        irAlPrimero();
        verNuevo = false;
    }

    public void adicionarAlFinal() {
        lista.adicionarFinal(PerroAdicionar);
        irAlPrimero();
        verNuevo = false;
    }

    public void eliminarPrimero() {
        lista.eliminarInicio();
    }

    public void eliminarFinal() {
        lista.eliminarFinal();
    }

    public void eliminarPosicion() {
        lista.eliminarPosicion(posicionBuscar);
    }

    public void cambiarLista() {
        lista.cambiarLista();
    }
    
    public void obtenervocal() {
        lista.obtenervocal();
        irAlPrimero();
        
    }
    
}

    
